BP=/mnt/projects/Colibri-Embedded/tools/blueprint/run.py

BP_META_DIR    		= 	./blueprint
BP_TEMPLATE_DIR		= 	$(BP_META_DIR)/templates
BP_BLUEPRINT 		= 	$(BP_META_DIR)/../blueprint.yaml

BP_GQL_CONFIG		=	$(BP_META_DIR)/graphql.yaml
BP_GQL_TYPE			= 	$(BP_META_DIR)/types.graphql
BP_GQL_CUSTOM		= 	$(BP_META_DIR)/user_extras.graphql
BP_VUEJS_PAGES 		= 	$(BP_META_DIR)/pages.yaml
BP_AUTHORIZATION 	= 	$(BP_META_DIR)/authorization.yaml

BP_SUGGESTED 		= 	$(BP_META_DIR)/types_bread.graphql \
						$(BP_META_DIR)/schema_bread.graphql

.PHONY: blueprint bp-model

bp-model: $(BP_GQL_TYPE) $(BP_USER_TYPES) $(BP_GQL_CONFIG)
	$(BP) graphql/model $(BP_META_DIR) $(foreach arg,$^,-i $(arg)) -vvv

$(BP_SUGGESTED): bp-model

$(BP_GQL_CUSTOM):
	touch $@

blueprint: $(BP_GQL_TYPE) $(USER_TYPES) $(BP_GQL_CONFIG) $(BP_SUGGESTED) $(BP_GQL_CUSTOM) $(BP_VUEJS_PAGES) $(BP_AUTHENTICATION) $(BP_AUTHORIZATION)
	$(BP) $(BP_BLUEPRINT) $(foreach arg,$^,-i $(arg)) -T $(BP_TEMPLATE_DIR) -vvv --remove-untracked