# Base name of distribution and release files
NAME	=	fabui

# Version is read from first paragraph of REAMDE file
VERSION		?=	$(shell grep '^DEMO-UI [0-9]\+\.[0-9]\+' README.md README.md | head -n1 | cut -d' ' -f2)
# If no version is found use 0.<date>.<time> generated version
ifeq ($(VERSION),)
VERSION = 0.$(shell date +%Y%m%d.%H%M)
endif

# Priority for colibri bundle
PRIORITY	?= 090

# FABUI license
LICENSE		?= GPLv2

# OS flavour identifier
OS_FLAVOUR	?= colibri

# System paths
LIB_PATH		 ?= /var/lib/$(NAME)
SHARED_PATH		 ?= /usr/share/$(NAME)
COLIBRI_LIB_PATH ?= /var/lib/colibri
METADATA_PATH	 ?= $(COLIBRI_LIB_PATH)/bundle/$(NAME)
WWW_PATH		 ?= /var/www
MOUNT_BASE_PATH	 ?= /mnt
APP_PATH		 ?= $(SHARED_PATH)
LOG_PATH		 ?= /var/log/$(NAME)

TEMP_PATH	 	 ?= /tmp
BIGTEMP_PATH	 ?= $(MOUNT_BASE_PATH)/bigtemp
USERDATA_PATH	 ?= $(MOUNT_BASE_PATH)/userdata
DB_PATH			 ?= $(LIB_PATH)
USB_MEDIA_PATH	 ?= /run/media
LOCALE_PATH		 ?= $(APP_PATH)/locale

WEB_FRAMEWORK_PATH ?= application/

# DEMO-UI parameters
SERIAL_PORT 	?= /dev/ttyAMA0
SERIAL_BAUD		?= 250000

# OS paths
PHP_CONFIG_FILE_SCANDIR ?= /etc/php/conf.d/
CRON_FOLDER 			?= /var/spool/cron/crontabs/

# Logging
LOG_LEVEL ?= INFO

# Build/Install paths
TEMP_DIR 		= ./temp
BDATA_DIR 		= $(TEMP_DIR)/bdata
BDATA_STAMP		= $(TEMP_DIR)/.bdata_stamp
APP_BUNDLE		= $(PRIORITY)-$(NAME)-v$(VERSION).cb

DESTDIR 		?= $(BDATA_DIR)

OS_FILES_DIR	= ./os

# This is not a mistake. OS_STAMP is general dependency used by bundle rule
OS_STAMP		= $(TEMP_DIR)/.os_$(OS_FLAVOUR)_stamp
# OS_COLIBRI_STAMP is specific stamp used in case OS_FLAVOUR is colibri
OS_COLIBRI_STAMP= $(TEMP_DIR)/.os_colibri_stamp
OS_COMMON_STAMP	= $(TEMP_DIR)/.os_common_stamp

# User/group
WWW_DATA_NAME	= www-data
WWW_DATA_UID 	= 33
WWW_DATA_GID 	= 33
WWW_DATA_GROUPS	= wheel,dialout,tty,plugdev,video

# Tools
INSTALL			?= install
FAKEROOT 		?= fakeroot
FAKEROOT_ENV 	= $(FAKEROOT) -s $(TEMP_DIR)/.fakeroot_env -i $(TEMP_DIR)/.fakeroot_env --
MKSQUASHFS		?= mksquashfs
BUNDLE_COMP		?= lzo

########################### Makefile rules #############################
.PHONY: locale clean distclean bundle check-tools docs

all: check-tools $(APP_BUNDLE)

clean:
	rm -rf $(TEMP_DIR)
	rm -rf $(CONFIG_FILES)
	rm -rf $(DB_FILES)

distclean: clean
	rm -rf *.cb
	rm -rf *.cb.md5sum
	rm -f $(GENERATED_FILES)

check-tools:
	@which fakeroot > /dev/null
	@echo "Looking for fakeroot: FOUND"
	
	@which mksquashfs > /dev/null
	@echo "Looking for mksquashfs: FOUND"
		
	@which php > /dev/null
	@echo "Looking for php: FOUND"
	
	@which npm > /dev/null
	@echo "Looking for npm: FOUND"
	
	@which composer > /dev/null
	@echo "Looking for composer: FOUND"

#~ 	@which sqlite3 > /dev/null
#~ 	@echo "Looking for sqlite3: FOUND"
	
#~ 	which pojson
#~ 	@echo "Looking for pojson: FOUND"

install: $(BDATA_STAMP) $(OS_COMMON_STAMP) $(OS_STAMP)
	echo "Install to $(DESTDIR)"
	
bundle: check-tools distclean $(APP_BUNDLE)

$(TEMP_DIR):
	mkdir -p $@

$(BDATA_DIR):
	mkdir -p $@
		
$(APP_BUNDLE): $(BDATA_DIR) $(BDATA_STAMP) $(OS_COMMON_STAMP) $(OS_STAMP)
	$(FAKEROOT_ENV) $(MKSQUASHFS) $(BDATA_DIR) $@ -noappend -comp $(BUNDLE_COMP) -b 512K -no-xattrs
	md5sum $@ > $@.md5sum

$(BDATA_STAMP): $(TEMP_DIR) $(DESTDIR) $(DB_FILES) $(GENERATED_FILES)
#	Add metadata
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)/$(METADATA_PATH)
#	metadata/info
	$(FAKEROOT_ENV) echo "name: $(NAME)" >> $(DESTDIR)/$(METADATA_PATH)/info
	$(FAKEROOT_ENV) echo "version: $(VERSION)" >> $(DESTDIR)/$(METADATA_PATH)/info
	$(FAKEROOT_ENV) echo "build-date: $(shell date +%Y-%m-%d)" >> $(DESTDIR)/$(METADATA_PATH)/info
#	metadata/packages
	$(FAKEROOT_ENV) echo "$(NAME): $(VERSION)" >> $(DESTDIR)/$(METADATA_PATH)/packages
#	metadata/licenses
	$(FAKEROOT_ENV) echo "$(NAME): $(LICENSE)" >> $(DESTDIR)/$(METADATA_PATH)/licenses
#	license files
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)/usr/share/licenses/$(NAME)
	$(FAKEROOT_ENV) cp LICENSE $(DESTDIR)/usr/share/licenses/$(NAME)
#   Install bundle helper scripts
	$(FAKEROOT_ENV) cp -R $(OS_FILES_DIR)/colibri/meta/* $(DESTDIR)/$(METADATA_PATH)/
#|username |uid |group |gid |password |home |shell |groups |comment
	$(FAKEROOT_ENV) echo "$(WWW_DATA_NAME) $(WWW_DATA_UID) $(WWW_DATA_NAME) $(WWW_DATA_GID) * /var/www /bin/sh $(WWW_DATA_GROUPS) Web Data" > $(DESTDIR)$(METADATA_PATH)/user_table
# 	Logs
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)/var/log/app
########################################################################
#	Application data
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)/$(WWW_PATH)
	$(FAKEROOT_ENV) cp -aR $(WEB_FRAMEWORK_PATH)/* 		$(DESTDIR)/$(WWW_PATH)
	$(FAKEROOT_ENV) cp $(WEB_FRAMEWORK_PATH)/.env.production $(DESTDIR)/$(WWW_PATH)
# 	Remove development files
	$(FAKEROOT_ENV) rm -f $(DESTDIR)/$(WWW_PATH)/database/database.sqlite
	$(FAKEROOT_ENV) rm -rf $(DESTDIR)/$(WWW_PATH)/node_modules
	$(FAKEROOT_ENV) rm -rf $(DESTDIR)/$(WWW_PATH)/tests
	$(FAKEROOT_ENV) rm -f $(DESTDIR)/$(WWW_PATH)/composer.json
	$(FAKEROOT_ENV) rm -f $(DESTDIR)/$(WWW_PATH)/composer.lock
	$(FAKEROOT_ENV) rm -f $(DESTDIR)/$(WWW_PATH)/package.json
	$(FAKEROOT_ENV) rm -f $(DESTDIR)/$(WWW_PATH)/phpunit.xml
#	Move logs to /var/log
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)/$(LOG_PATH)
	$(FAKEROOT_ENV) touch $(DESTDIR)/$(LOG_PATH)/lumen.log
	$(FAKEROOT_ENV) chown $(WWW_DATA_UID):$(WWW_DATA_GID) $(DESTDIR)/$(LOG_PATH)/lumen.log
	$(FAKEROOT_ENV) ln -sfv $(LOG_PATH)/lumen.log $(DESTDIR)/$(WWW_PATH)/storage/logs/lumen.log 
#	Switch to production environment
	$(FAKEROOT_ENV) ln -sfv .env.production $(DESTDIR)/$(WWW_PATH)/.env
# 	Fix permissions
	$(FAKEROOT_ENV) chown -R $(WWW_DATA_UID):$(WWW_DATA_GID) $(DESTDIR)$(WWW_PATH)
#	Nginx config
	$(FAKEROOT_ENV) install -d -m 0755 $(DESTDIR)/etc/nginx/sites-available
	$(FAKEROOT_ENV) $(INSTALL) -D -m 0775 $(OS_FILES_DIR)/common/lumen.site \
		$(DESTDIR)/etc/nginx/sites-available/lumen
		
	$(FAKEROOT_ENV) install -d -m 0755 $(DESTDIR)/etc/nginx/sites-enabled
	$(FAKEROOT_ENV) ln -s /etc/nginx/sites-available/lumen $(DESTDIR)/etc/nginx/sites-enabled/lumen
	
$(OS_COMMON_STAMP):
# 	Avahi service
	$(FAKEROOT_ENV) install -d -m 0775 $(DESTDIR)/etc/avahi/services
#	Sudoers fabui rule
	$(FAKEROOT_ENV) install -d -m 0750 $(DESTDIR)/etc/sudoers.d
# CRON file
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)$(CRON_FOLDER)
# Network manager
	$(FAKEROOT_ENV) install -d -m 0775 $(DESTDIR)/etc/default

	$(FAKEROOT_ENV) install -d -m 0775 $(DESTDIR)/etc/connman
	$(FAKEROOT_ENV) install -D -m 0644 $(OS_FILES_DIR)/common/connman/main.conf $(DESTDIR)/etc/connman/main.conf
# 	Create a stamp file
	touch $@
	
$(OS_COLIBRI_STAMP):
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)/etc/init.d
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)/etc/rc.d/rc.firstboot.d
	
	$(FAKEROOT_ENV) $(INSTALL) -D -m 0775 $(OS_FILES_DIR)/colibri/network.first \
		$(DESTDIR)/etc/firstboot.d/network
	$(FAKEROOT_ENV) ln -fs ../../firstboot.d/lumen \
		$(DESTDIR)/etc/rc.d/rc.firstboot.d/S10lumen
		
	$(FAKEROOT_ENV) $(INSTALL) -D -m 0775 $(OS_FILES_DIR)/colibri/lumen.first \
		$(DESTDIR)/etc/firstboot.d/lumen
	$(FAKEROOT_ENV) ln -fs ../../firstboot.d/network \
		$(DESTDIR)/etc/rc.d/rc.firstboot.d/S09network
		
# 	Create a stamp file
	touch $@

include blueprint.mk