<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/* Authentication */
$router->post('auth/token',  ['uses' => 'AuthController@authenticate']);
$router->post('auth/token/refresh',  ['uses' => 'AuthController@refresh']);

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->post('auth/token/revoke',  ['uses' => 'AuthController@revoke']);
});

$router->group(['prefix' => 'v1'], function () use ($router) {

	/* public endoints */
	$router->get('setup',  ['uses' => 'SetupController@done']);
	$router->post('setup',  ['uses' => 'SetupController@setup']);

	/* protected endpoints */
	$router->group(['middleware' => 'auth'], function () use ($router) {

		/* user related endpoints */
		$router->get('me', ['uses' => 'UserController@profile']);

		// Local Storage
		$router->group(['prefix' => 'storages/local'], function () use ($router) {
			//$router->get('/', 					['uses' => 'LocalStorageController@index']);
			//$router->get('/files/{fileId}/browse', 	['uses' => 'LocalStorageController@index']);
			$router->get('/root',					['uses' => 'LocalStorageController@root']);
			$router->get('/files/{fileId}', 		['uses' => 'LocalStorageController@show']);
			$router->post('/files/upload',			['uses' => 'LocalStorageController@upload']);
			$router->put('/files/{fileId}', 		['uses' => 'LocalStorageController@update']);
			$router->put('/files', 					['uses' => 'LocalStorageController@create']);
			$router->delete('/files/{fileId}', 		['uses' => 'LocalStorageController@destroy']);
		});

		$router->group(['prefix' => 'storages/sdcard'], function () use ($router) {

		});

		$router->group(['prefix' => 'storages/usb'], function () use ($router) {

		});

		// Users
		$router->group(['prefix' => 'users'], function () use ($router) {
			$router->get('/',              ['uses' => 'UserController@index']);
			$router->get('/{userId}',      ['uses' => 'UserController@show']);
			$router->post('/{userId}',     ['uses' => 'UserController@update']);
			$router->put('/',              ['uses' => 'UserController@create']);
			$router->delete('/{userId}',   ['uses' => 'UserController@destroy']);
		});

		// Settings

		// Tasks
		$router->group(['prefix' => 'tasks'], function () use ($router) {
			$router->get('/',              ['uses' => 'TaskController@index']);
			$router->get('/{taskId}',      ['uses' => 'TaskController@show']);
			$router->post('/{taskId}',     ['uses' => 'TaskController@update']);
			$router->put('/',              ['uses' => 'TaskController@create']);
			$router->delete('/{taskId}',   ['uses' => 'TaskController@destroy']);
		});

	});

});

$router->get('/{any:.*}', ['uses' => 'SpaController@index']);
