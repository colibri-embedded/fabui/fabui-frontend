<?php

namespace App\Policies;

// section:use_extra
use Log;
// endsection:use_extra

use App\User;
use App\Task;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * If you wish to authorize an action before any policy
     * rule check, you can to it here.
     *
     * @param  \App\User  $user
     * @param             $model
     * @return mixed
     **/
    public function before(User $user, $ability)
    {
        // section:custom_before_rules
        if( $user->isBackend() )
            return TRUE;
        // endsection:custom_before_rules
    }

    /**
     * Determine whether the user can list the tasks.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function browse(User $user)
    {
        $have_access = TRUE;
        $have_permission = $user->isAdmin() || $user->isManager() || $user->isObserver() || $user->isBackend();

        // section:custom_view_rules
        // endsection:custom_view_rules

        return ($have_access && $have_permission);
    }

    /**
     * Determine whether the user can view a task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $model
     * @return mixed
     */
    public function view(User $user, Task $model)
    {
        $have_access = FALSE;

        $owner_id = $user->id;
        $model_owner_id = $model->user->id;
        // Owner access
        if ($owner_id == $model_owner_id) {
            $have_access = TRUE;
        }
        // ROOT access
        if ( $user->isBackend() && $user->isAdmin() ) {
            $have_access = TRUE;
        }

        $have_permission = $user->isAdmin() || $user->isManager() || $user->isObserver() || $user->isBackend();

        // section:custom_view_rules
        // endsection:custom_view_rules

        return ($have_access && $have_permission);
    }

    /**
     * Determine whether the user can create tasks.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $have_access = TRUE;
        $have_permission = $user->isAdmin() || $user->isManager() || $user->isBackend();

        // section:custom_create_rules
        // endsection:custom_create_rules

        return ($have_access && $have_permission);
    }

    /**
     * Determine whether the user can update the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $model
     * @return mixed
     */
    public function update(User $user, Task $model)
    {
        $have_access = FALSE;

        $owner_id = $user->id;
        $model_owner_id = $model
                                ->user->id;
        // Owner access
        if ($owner_id == $model_owner_id) {
            $have_access = TRUE;
        }

        $have_permission = $user->isAdmin() || $user->isManager() || $user->isBackend();

        // section:custom_update_rules
        // endsection:custom_update_rules

        return ($have_access && $have_permission);
    }

    /**
     * Determine whether the user can delete the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $model
     * @return mixed
     */
    public function delete(User $user, Task $model)
    {
        $have_access = FALSE;

        $owner_id = $user->id;
        $model_owner_id = $model
                                ->user->id;
        // Owner access
        if ($owner_id == $model_owner_id) {
            $have_access = TRUE;
        }
        // ROOT access
        if ( $user->isBackend() && $user->isAdmin() ) {
            $have_access = TRUE;
        }

        $have_permission = $user->isAdmin() || $user->isManager() || $user->isBackend();

        // section:custom_delete_rules
        // endsection:custom_delete_rules

        return ($have_access && $have_permission);
    }
}