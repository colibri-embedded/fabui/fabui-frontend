<?php

namespace App\Policies;

// section:use_extra
// endsection:use_extra

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * If you wish to authorize an action before any policy
     * rule check, you can to it here.
     *
     * @param  \App\User  $user
     * @param             $model
     * @return mixed
     **/
    public function before(User $user, $ability)
    {
        // section:custom_before_rules
        if( $user->isBackend() )
            return TRUE;
        // endsection:custom_before_rules
    }

    /**
     * Determine whether the user can list the users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function browse(User $user)
    {
        $have_access = TRUE;
        $have_permission = $user->isAdmin() || $user->isManager() || $user->isObserver() || $user->isBackend();

        // section:custom_view_rules
        // endsection:custom_view_rules

        return ($have_access && $have_permission);
    }

    /**
     * Determine whether the user can view a user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        $have_access = FALSE;

        $owner_id = $user->id;
        // Owner access
        if ($owner_id == $model_owner_id) {
            $have_access = TRUE;
        }
        // ROOT access
        if ( $user->isBackend() && $user->isAdmin() ) {
            $have_access = TRUE;
        }

        $have_permission = $user->isAdmin() || $user->isManager() || $user->isObserver() || $user->isBackend();

        // section:custom_view_rules
        // endsection:custom_view_rules

        return ($have_access && $have_permission);
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $have_access = TRUE;
        $have_permission = $user->isAdmin() || $user->isManager() || $user->isBackend();

        // section:custom_create_rules
        // endsection:custom_create_rules

        return ($have_access && $have_permission);
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        $have_access = FALSE;

        $owner_id = $user->id;
        $model_owner_id = $model->id;
        // Owner access
        if ($owner_id == $model_owner_id) {
            $have_access = TRUE;
        }

        $have_permission = $user->isAdmin() || $user->isManager() || $user->isBackend();

        // section:custom_update_rules
        // endsection:custom_update_rules

        return ($have_access && $have_permission);
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        $have_access = FALSE;

        $owner_id = $user->id;
        $model_owner_id = $model->id;
        // Owner access
        if ($owner_id == $model_owner_id) {
            $have_access = TRUE;
        }
        // ROOT access
        if ( $user->isBackend() && $user->isAdmin() ) {
            $have_access = TRUE;
        }

        $have_permission = $user->isAdmin() || $user->isManager() || $user->isBackend();

        // section:custom_delete_rules
        // endsection:custom_delete_rules

        return ($have_access && $have_permission);
    }
}