<?php

namespace App;

// section:use_extra
// endsection:use_extra

use Illuminate\Database\Eloquent\Model;

class RefreshToken extends Model
{
    // section:traits_extra
    // endsection:traits_extra
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['owner_id', 'refresh_token', 'revoked', 'expires_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Accessors
     */
    
    
    // section:accessors_implementation
    // endsection:accessors_implementation

    /**
     * Relations
     */
    
    public static function boot() {
        parent::boot();

        self::deleting(function ($refreshtoken) {
            

            // section:custom_delete_handling
            // endsection:custom_delete_handling
        });

        // section:custom_boot_helpers
        // endsection:custom_boot_helpers
    }

    // section:custom_helpers
    // endsection:custom_helpers
}