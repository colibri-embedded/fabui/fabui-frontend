<?php

namespace App;

// section:use_extra
// endsection:use_extra

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    // section:traits_extra
    // endsection:traits_extra
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'controller', 'status', 'started_at', 'finished_at', 'user_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Accessors
     */
    
    
    // section:accessors_implementation
    // endsection:accessors_implementation

    /**
     * Relations
     */
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function files()
    {
        return $this->belongsToMany('App\File', 'task_files', 'task_id', 'file_id');
    }
    
    public static function boot() {
        parent::boot();

        self::deleting(function ($task) {
            

            // section:custom_delete_handling
            // endsection:custom_delete_handling
        });

        // section:custom_boot_helpers
        // endsection:custom_boot_helpers
    }

    // section:custom_helpers
    // endsection:custom_helpers
}