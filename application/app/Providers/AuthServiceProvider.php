<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Log;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        Gate::define('users.view', 'App\Policies\UserPolicy@view');
        Gate::define('users.create', 'App\Policies\UserPolicy@create');
        Gate::define('users.update', 'App\Policies\UserPolicy@update');
        Gate::define('users.delete', 'App\Policies\UserPolicy@delete');

        Gate::define('settings.view', 'App\Policies\SettingPolicy@view');
        Gate::define('settings.create', 'App\Policies\SettingPolicy@create');
        Gate::define('settings.update', 'App\Policies\SettingPolicy@update');
        Gate::define('settings.delete', 'App\Policies\SettingPolicy@delete');

        Gate::define('files.view', 'App\Policies\FilePolicy@view');
        Gate::define('files.create', 'App\Policies\FilePolicy@create');
        Gate::define('files.update', 'App\Policies\FilePolicy@update');
        Gate::define('files.delete', 'App\Policies\FilePolicy@delete');

        Gate::define('tasks.view', 'App\Policies\TaskPolicy@view');
        Gate::define('tasks.create', 'App\Policies\TaskPolicy@create');
        Gate::define('tasks.update', 'App\Policies\TaskPolicy@update');
        Gate::define('tasks.delete', 'App\Policies\TaskPolicy@delete');

        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
        $this->app['auth']->viaRequest('jwt-auth', function ($request) {
            return $request->auth;
        });
    }
}
