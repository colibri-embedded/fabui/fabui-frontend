<?php

namespace App;

// section:use_extra
// endsection:use_extra

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    // section:traits_extra
    protected $appends = ['path'];
    // endsection:traits_extra
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'filename', 'ext', 'is_directory', 'is_shared', 'parent_id', 'user_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Accessors
     */
    
    
    // section:accessors_implementation

    private function getFilePath($file, $path = [])
    {
        if($file) {
            $element = [
                'id' => $file->id,
                'name' => $file->name
            ];
            //array_unshift($path, $element);
            $path = array_merge($path, array($element));
            $path = $this->getFilePath($file->parent, $path);
        }

        return $path;
    }

    public function getPathAttribute()
    {
        return array_reverse($this->getFilePath($this->parent));
    }
    // endsection:accessors_implementation

    /**
     * Relations
     */
    
    public function parent()
    {
        return $this->belongsTo('App\File', 'parent_id');
    }
    
    public function files()
    {
        return $this->hasMany('App\File', 'parent_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public static function boot() {
        parent::boot();

        self::deleting(function ($file) {
            
            foreach($file->files as $file) {
                $file->delete();
            }

            // section:custom_delete_handling
            if(!$file->is_directory) {
                $full_filename = storage_path($file->filename);
                if(!is_dir($full_filename) && file_exists($full_filename)) {
                    unlink($full_filename);
                }
            }
            // endsection:custom_delete_handling
        });

        // section:custom_boot_helpers
        // endsection:custom_boot_helpers
    }

    // section:custom_helpers
    // endsection:custom_helpers
}