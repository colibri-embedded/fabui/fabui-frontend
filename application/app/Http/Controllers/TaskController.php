<?php

namespace App\Http\Controllers;

use Log;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    
    public function index(Request $request) 
    {
        $user = $request->user();
        $this->authorize('tasks.view', $user);
        // authorized

        $task = [];
        Log::info('tasks all');
        return response()->json($task, 200);
    }

    public function show(Request $request, $taskId) 
    {
        $task = Task::findOrFail($taskId);
        $this->authorize('tasks.view', $task);
        // authorized
        return response()->json($task, 200);
    }

    public function update(Request $request, $taskId)
    {
        $task = Task::findOrFail($taskId);
        $this->authorize('tasks.update', $task);
        // authorized
        $this->validate($request, [
            'status' => 'required|string|in:ABORTED,FINISHED,FAILED'
        ]);
        // validated
        $new_data = $request->only(['status']);
        $new_data['finished_at'] = Carbon::now();

        $task->update($new_data);
        $task = Task::find($task->id);

        return response()->json($task, 200);
    }

    public function create(Request $request)
    {
        $user = $request->user();
        $this->authorize('tasks.create', $user);
        // authorized
        $this->validate($request, [
            'user_id'=> 'required|integer',
            'type'  => 'required|string',
            'controller'  => 'required|string',
            'started_at' => 'date',
            'finished_at' => 'date'
        ]);
        // validated
        $data = $request->all();

        // ensure the correct user_id is used
        if($user->isBackend()) {
            if( !array_key_exists('user_id', $data) )
                return response()->json([
                    'error' => 400,
                    'message' => 'Bad request'
                ], 400);
        } else {
            $data['user_id'] = $user->id;
        }

        if( array_key_exists('user_id', $data) ) {
            if(!$user->isBackend()) {
                $data['user_id'] = $user->id;
            }
        } else {
            if(!$user->isBackend()) {
                $data['user_id'] = $user->id;   
            }
        }

        $task = Task::create($data);
        // load the new entry with all fields
        $task = Task::find($task->id);

        return response()->json($task, 201);
    }

    public function destroy(Request $request, $taskId)
    {
        $task = Task::findOrFail($taskId);
        $this->authorize('tasks.delete', $task);
        // authorized
        $deleted = $task->toArray();
        $task->delete();
        return response()->json($deleted, 200);
    }
}
