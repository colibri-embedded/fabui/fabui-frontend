<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use App\User;
use App\RefreshToken;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller 
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Create a new token.
     * 
     * @param  \App\User   $user
     * @return string
     */
    protected function jwt(User $user) {

        //$expires_in = 60*60;
        $expires_in = 30;

        $payload = [
            'iss' => "fabui-jwt",   // Issuer of the token
            'sub' => $user->id,     // Subject of the token
            'rle' => $user->role,   // Role claim
            'iat' => time(),                // Time when JWT was issued. 
            'exp' => time() + $expires_in   // Expiration time
        ];

        $stored = RefreshToken::where('owner_id', $user->id)->first();
        if(!$stored) {
            while(true) {
                $refresh_token = sha1(time()); // TODO: generate and store refresh token  
                $existing = RefreshToken::where('refresh_token', $user->id)->first();
                if(!$existing)
                    break;
            }

            $now = Carbon::now();
            $expires_at = $now->addYears(1);

            RefreshToken::create([
                'owner_id' => $user->id,
                'refresh_token' => $refresh_token,
                'revoked' => false,
                'expires_at' => $expires_at 
            ]);

        } else {
            $refresh_token = $stored->refresh_token;
            if($stored->revoked) {
                $stored->revoked = false;
                $stored->save();
            }
        }

        //sha1(time())
        //$refresh_token = ''; // TODO: generate and store refresh token
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return [ 
        	'access_token' => JWT::encode($payload, env('JWT_SECRET')),
        	'expires_in' => $expires_in*1000, // in milliseconds
            'refresh_token' => $refresh_token,
            'token_type' => 'Bearer',
        ];
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * 
     * @param  \App\User   $user 
     * @return mixed
     */
    public function authenticate(Request $request) {
        $this->validate($this->request, [
            'grant_type'=> 'required',
            'username'  => 'required|email',
            'password'  => 'required'
        ]);
        // Find the user by email
        $user = User::where('email', $this->request->input('username'))->first();
        if (!$user) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error' => 'Email does not exist.'
            ], 400);
        }

        // Verify the password and generate the token
        if (Hash::check($this->request->input('password'), $user->password)) {
            return response()->json(
                $this->jwt($user)
            , 200);
        }
        // Bad Request response
        return response()->json([
            'error' => 'Email or password is wrong.'
        ], 400);
    }

    public function refresh(Request $request)
    {
        $this->validate($this->request, [
            'grant_type'=> 'required',
            'refresh_token'  => 'required'
        ]);

        $token = RefreshToken::where('refresh_token', $this->request->input('refresh_token'))->first();
        if($token) {

            if($token->revoked) {
                // Bad Request response
                return response()->json([
                    'error' => 'Revoked refresh_token.'
                ], 401);
            }

            $user = User::where('id', $token->owner_id)->first();
            if($user) {
                return response()->json(
                    $this->jwt($user)
                , 200);
            }
        }

        // Bad Request response
        return response()->json([
            'error' => 'Invalid refresh_token.'
        ], 401);
    }

    public function revoke(Request $request)
    {
        $user = $request->auth;
        $token = RefreshToken::where('owner_id', $user->id)->first();
        if($token) {
            $token->revoked = true;
            $token->save();
            return response()->json([], 204);
        }
        return response()->json([
            'error' => 'refresh_token not found.'
        ], 401);
    }
}