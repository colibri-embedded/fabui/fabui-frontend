<?php

namespace App\Http\Controllers;

use Log;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function profile(Request $request) 
    {
        //if(Auth::check()) {
        $user = $request->user();
        //Log::info($request->ip());
        return response()->json($user, 200);            
        //}

    }

    public function index(Request $request) 
    {

    }

    public function show(Request $request, $userId) 
    {
        $user = User::findOrFail($userId);
        $this->authorize('users.view', $user);
    }

    public function update(Request $request, $userId)
    {
        $user = User::findOrFail($userId);
        $this->authorize('users.update', $user);
    }

    public function create(Request $request)
    {
        $this->authorize('users.create');
        // authorized
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|string',
            'role' => 'required|string|in:ADMIN|MANAGER|OBSERVER',
            'avatar_url' => 'string'
        ]);

        $data = $request->only([
            'name', 
            'email', 
            'password',
            'role',
            'avatar_url'
        ]);

        $user = User::create($data);
        // load the new entry with all fields
        $user = User::find($user->id);
        return response()->json($user, 201);
    }

    public function destroy(Request $request, $userId)
    {
    	$user = User::findOrFail($userId);
        $this->authorize('users.delete', $user);
    }
}
