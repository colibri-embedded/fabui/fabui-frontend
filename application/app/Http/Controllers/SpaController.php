<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SpaController extends Controller
{
    public function index(Request $request) {
    	if($request->wantsJson()) {
    		abort(404, 'Path not found');
    	}


		$max_upload = (int)(ini_get('upload_max_filesize'));
		$max_post = (int)(ini_get('post_max_size'));
		//$memory_limit = (int)(ini_get('memory_limit'));
		$upload_mb = min($max_upload, $max_post) * 1024*1024;
		$max_file_uploads = (int)(ini_get('max_file_uploads'));

    	return view('spa', [
    		'max_file_size' => $upload_mb,
    		'max_file_uploads' => $max_file_uploads
    	]);
    }
}
