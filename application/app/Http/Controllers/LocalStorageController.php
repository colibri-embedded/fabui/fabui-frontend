<?php

namespace App\Http\Controllers;

use Log;
use App\File;
use Illuminate\Http\Request;

class LocalStorageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request, $fileId = null)
    {
        // TODO: authorization
        $user = $request->user();
        $data = File::select([
            'id',
            'name',
            'ext',
            'filename',
            'is_directory',
            'is_shared',
            'parent_id',
            'user_id'
        ])->where('user_id', $user->id)->get();

        return response()->json($data, 200);
    }

    public function root(Request $request)
    {
        // TODO: authorization
        $user = $request->user();
        $files = File::where('parent_id', null)->where('user_id', $user->id)->get();
        $data = [
            'id' => null,
            'name' => 'ROOT',
            'files' => $files,
            'parent_id' => null,
            'user_id' => $user->id,
            'is_directory' => true,
            'is_shared' => false,
            'path' => []
        ];
        return response()->json($data, 200);
    }

    public function show($fileId)
    {
        $file = File::with('files')->where('id', $fileId)->firstOrFail();
        $this->authorize('files.view', $file);
        // authorized
        return response()->json($file, 200);
    }

    public function update(Request $request, $fileId)
    {
        $file = File::findOrFail($fileId);
        $this->authorize('files.update', $file);
        // authorized
        $this->validate($request, [
            // 'user_id'=> 'integer',
            'parent_id'=> 'integer|nullable',
            'name' => 'required|string',
            'is_shared'  => 'boolean'
        ]);

        $new_data = $request->only(['name', 'is_shared', 'parent_id']);
        
        $file->update($new_data);
        $file = Task::find($file->id);

        return response()->json($file, 200);
    }

    public function upload(Request $request)
    {
        $user = $request->user();
        $this->authorize('files.create', $user);
        // authorized
        $this->validate($request, [
            'user_id'=> 'integer',
            'parent_id'=> 'required|nullable',
            'is_shared'  => 'boolean',
            'files.*' => 'required|file'
        ]);

        $files = $request->file('files');

        $data = $request->only(['parent_id', 'is_shared', 'user_id']);
        if($user->isBackend()) {
            if( !array_key_exists('user_id', $data) )
                return response()->json([
                    'error' => 400,
                    'message' => 'Bad request'
                ], 400);
        } else {
            $data['user_id'] = $user->id;
        }

        if( array_key_exists('user_id', $data) ) {
            if(!$user->isBackend()) {
                $data['user_id'] = $user->id;
            }
        } else {
            if(!$user->isBackend()) {
                $data['user_id'] = $user->id;   
            }
        }

        // Fix formData converting everything to strings
        $data['user_id'] = intval($data['user_id']);

        $data['parent_id'] = intval($data['parent_id']);
        if($data['parent_id'] == '0')
            $data['parent_id'] = null;

        if(array_key_exists('is_shared', $data)) {
            $data['is_shared'] = intval($data['is_shared']);
        }

        // TODO: file-size check
        // TODO: file-type check (extension)

        $uploadPath = 'app/files/';
        $result = [];
        foreach($files as $file_upload) {
            $orig_filename = $file_upload->getClientOriginalName();
            $data['name'] = pathinfo($orig_filename, PATHINFO_FILENAME);
            $data['ext'] = '.' . pathinfo($orig_filename, PATHINFO_EXTENSION);
            $destinationPath = storage_path($uploadPath);

            // Generate a unique hash filename
            $unique = time() . $orig_filename;
            $destinationFileName = md5($unique) . $data['ext'];
            $data['filename'] = $uploadPath . $destinationFileName;

            // Create destination folder
            if(!file_exists($destinationPath)) {
                mkdir($destinationPath, 0755, TRUE);
            }
            $file_upload->move($destinationPath, $destinationFileName);

            $file = File::create($data);
            $result[] = File::find($file->id);
        }

        return response()->json($result, 201);
    }

    public function create(Request $request)
    {
        $user = $request->user();
        $this->authorize('files.create', $user);
        // authorized
        $this->validate($request, [
            'user_id'=> 'integer',
            'parent_id'=> 'integer|nullable',
            'name'  => 'required|string',
            'is_directory'  => 'boolean',
            'is_shared'  => 'boolean'
        ]);

        $data = $request->only([
            'parent_id',
            'name', 
            'is_directory', 
            'is_shared'
        ]);

        if($user->isBackend()) {
            if( !array_key_exists('user_id', $data) )
                return response()->json([
                    'error' => 400,
                    'message' => 'Bad request'
                ], 400);
        } else {
            $data['user_id'] = $user->id;
        }

        if( array_key_exists('user_id', $data) ) {
            if(!$user->isBackend()) {
                $data['user_id'] = $user->id;
            }
        } else {
            if(!$user->isBackend()) {
                $data['user_id'] = $user->id;   
            }
        }

        $file = File::create($data);
        // load the new entry with all fields
        $file = File::find($file->id);
        return response()->json($file, 201);
    }

    public function destroy(Request $request, $fileId)
    {
        $file = File::findOrFail($fileId);
        $this->authorize('files.delete', $file);
        // authorized
        $deleted = $file->toArray();
        $file->delete();
        return response()->json($deleted, 200);
    }

}
