<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SetupController extends Controller
{
    public function setup(Request $request) 
    {

    }

    public function done()
    {
    	$setup_is_done = true;
    	return response()->json($setup_is_done, 200);
    }
}
