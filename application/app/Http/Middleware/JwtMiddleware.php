<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

use Log;

class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = null;
        $authorization = $request->header('Authorization');

        if($authorization) {
            $tmp = explode(' ', $authorization, 2);
            if(count($tmp) == 2) {
                $token = $tmp[1];
            }
        }
        
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'Access token not provided.'
            ], 401);
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'error' => 'Provided access token is expired.'
            ], 400);
        } catch(Exception $e) {
            return response()->json([
                'error' => 'An error while decoding token.'
            ], 400);
        }

        if($credentials->sub == 0 && $credentials->rle == "BACKEND") {
            $user = new User;
            $user->id = 1;
            $user->role = $credentials->rle;
        } else {
            $user = User::find($credentials->sub);
        }
        
        // Now let's put the user in the request class so that you can grab it from there
        $request->auth = $user;
        return $next($request);
    }
}