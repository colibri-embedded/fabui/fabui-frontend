<?php

namespace App;

// section:use_extra
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
// endsection:use_extra

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;

class User extends Model implements Authenticatable
{
    // section:traits_extra
    use AuthenticableTrait;
    // endsection:traits_extra
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'avatar_url', 'role'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * Accessors
     */
    
        
    public function isBackend()
    {
        return $this->role == 'BACKEND';
    }
        
    public function isAdmin()
    {
        return $this->role == 'ADMIN';
    }
        
    public function isManager()
    {
        return $this->role == 'MANAGER';
    }
        
    public function isObserver()
    {
        return $this->role == 'OBSERVER';
    }
        
    // section:accessors_implementation
    // endsection:accessors_implementation

    /**
     * Relations
     */
    
    public function files()
    {
        return $this->hasMany('App\File', 'user_id');
    }
    
    public function tasks()
    {
        return $this->hasMany('App\Task', 'user_id');
    }
    
    public function settings()
    {
        return $this->hasMany('App\Setting', 'user_id');
    }
    
    public static function boot() {
        parent::boot();

        self::deleting(function ($user) {
            
            foreach($user->files as $file) {
                $file->delete();
            }
            foreach($user->tasks as $task) {
                $task->delete();
            }
            foreach($user->settings as $setting) {
                $setting->delete();
            }

            // section:custom_delete_handling
            // endsection:custom_delete_handling
        });

        // section:custom_boot_helpers
        // endsection:custom_boot_helpers
    }

    // section:custom_helpers
    // endsection:custom_helpers
}