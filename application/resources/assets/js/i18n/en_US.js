export default {
	$vuetify: {
		'dataIterator': {
			'rowsPerPageText': 'Items per page:',
			'rowsPerPageAll': 'All',
			'pageText': '{0}-{1} of {2}',
			'noResultsText': 'No matching records found',
			'nextPage': 'Next page',
			'prevPage': 'Previous page'
		},
		'dataTable': {
			'rowsPerPageText': 'Rows per page:'
		},
		'noDataText': 'No data available',
	},
	setupWizard: {
		title: 'Setup',
		nextLabel: 'Next',
		prevLabel: 'Prev',
		finishLabel:  'Finish',
		firstNameLabel: 'First name',
		lastNameLabel: 'Last name',
		emailLabel: 'E-mail',
		passwordLabel: 'Password',
		welcomeLabel: 'Welcome',
		accountLabel: 'Account',
		printerLabel: 'Printer',
		networkLabel: 'Network',
	},
	filemanager: {
		title: 'File manager',
		uploadLabel: 'Upload',
		createFolder: {
			title: 'Create a new folder',
			label: 'New folder',
			defaultName: 'New folder',
			nameInputLabel: 'Folder name',
			nameErrorLabel: 'Folder name is required',
		}
	}
}