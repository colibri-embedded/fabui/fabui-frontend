import Vue from 'vue'
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)

import en_US from './en_US.js'

const messages = {
	en_US
}

const i18n = new VueI18n({
	locale: 'en_US', // set locale
	messages // set locale messages
})

export default i18n