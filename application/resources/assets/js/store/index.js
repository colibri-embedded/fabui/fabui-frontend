import createPersistedState from 'vuex-persistedstate'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import auth from '@js/auth/store'

const store = new Vuex.Store({
	plugins: [createPersistedState()],

	modules: {
		auth
	},

	actions: {
	},

	getters: {
	}

})

export default store