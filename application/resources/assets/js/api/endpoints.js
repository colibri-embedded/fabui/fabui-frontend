export default {
	/* Authentication */
	auth_token_url          : '/auth/token',
	auth_refresh_token_url  : '/auth/token/refresh',
	auth_revoke_url         : '/auth/token/revoke',
	get_user_url            : '/v1/me',
    
	/* API */
	setup_url               : '/v1/setup',
	storages_url            : '/v1/storages',

	files_root_url			: '/v1/storages/{{storage}}/root',
	files_query_url			: '/v1/storages/{{storage}}/files/{{id}}',
	files_update_url		: '/v1/storages/{{storage}}/files/{{id}}',
	files_delete_url		: '/v1/storages/{{storage}}/files/{{id}}',
	files_upload_url		: '/v1/storages/{{storage}}/files/upload',
	files_create_url		: '/v1/storages/{{storage}}/files',

}