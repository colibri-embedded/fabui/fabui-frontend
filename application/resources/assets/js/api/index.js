import axios from 'axios'
import Endpoints from './endpoints'
import Storage from './modules/storage'

const API = {

	setup: {
		check() {
			return new Promise( (resolve, reject) => {
				axios.get(Endpoints.setup_url)
					.then(response => {
						resolve(response.data)
					})
					.catch(response => {
						reject(response)
					})
			})
		},
	},

	storage: Storage

}

export default API