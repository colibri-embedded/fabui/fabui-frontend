import axios from 'axios'
import Endpoints from '@api/endpoints'

const Files = {
	allFiles(vars) {

		var storageId = 'local'
		var fileId = null

		if(vars.storage !== undefined)
			storageId = vars.storage

		if(vars.parent !== undefined)
			fileId = vars.parent

		console.log('allFiles call:', storageId, fileId)

		var url = Endpoints.storages_url + '/' + storageId
		if(fileId !== null) {
			url += '/' + fileId + '/browse'
		}

		return new Promise( (resolve, reject) => {
			axios.get(url)
				.then(response => {
					resolve({
						data: response.data,
						query: url
					})
				})
				.catch(response => {
					reject(response)
				})
		})
	},

	file(fileId) {

	},
}

export default Files