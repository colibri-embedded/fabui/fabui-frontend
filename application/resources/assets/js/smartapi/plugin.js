import SmartQuery from './smart-query'

import Mustache from 'mustache'
import {debounce} from 'throttle-debounce'
import defer from 'deferred'

export let _Vue
export let install = {
	installed: false
}

class SmartApi { 

	static install(Vue, options) {

		if (install.installed && _Vue === Vue) return
		install.installed = true

		_Vue = Vue


		const isDef = v => v !== undefined

		const registerInstance = (vm, callVal) => {
		
			let i = vm.$options._parentVnode
			if (isDef(i) && isDef(i = i.data) && isDef(i = i.registerRouteInstance)) {
				i(vm, callVal)
			}
		}

		Vue.mixin({
			beforeCreate () {
				if (isDef(this.$options.smartapi)) {
					this._smartapi = this.$options.smartapi
					this._smartapiRoot = this
					this._smartapi.init(this)
				} else {
					this._smartapiRoot = (this.$parent && this.$parent._smartapiRoot) || this
				}

				if (isDef(this.$options.api)) {
					//console.log('CREATE')
					this._smartapiData = this.$options.api
					var smartapi = this._smartapiRoot._smartapi
					let props = Object.getOwnPropertyNames(this._smartapiData)

					for(let x in props) {
						let propName = props[x]
						let query = this._smartapiData[propName]

						smartapi.queries[propName] = new SmartQuery(this, smartapi, propName, {})
					}

					//console.log('QUERIES', this.queries)

				}
				registerInstance(this, this)
			},

			created() {

				if (isDef(this.$options.api)) {
					//console.log('found: smartapi', this)

					let props = Object.getOwnPropertyNames(this._smartapiData)
					var smartapi = this._smartapiRoot._smartapi
					for(let x in props) {
						let propName = props[x]
						let query = this._smartapiData[propName]

						//console.log('SMART', smartapi)

						if( (query.url !== undefined)
						&&	(query.url_handler !== undefined)) {
							// 
							console.error(' "url" and "url_handler" used at the same time for property "'+ propName + '"')
							continue
						}

						if(query.url_handler !== undefined) {
							if(typeof query.url_handler !== 'function') {
								console.error('"url_handler" must be a function for property "'+ propName + '"')
								continue
							}

							if(smartapi.axios === null) {
								console.error('"url_handler" is used but axios is not passed as smartapi option')
								continue
							}
						}

						var preFetch = true

						let queryCallback = (value, oldValue) => {
							console.log('variables changed', value, oldValue, propName)
							if (value !== oldValue) {
								/*if(this.$smartapi.auth) {
									console.log('have $smartapi.auth')
									this.$smartapi.auth.check()
										.then(result => {
											console.log('have $smartapi.auth check finished')
											this.$smartapi._getQuery.call(this, propName)
										})
										.catch(error => {
											console.error('smartapi.query: auth failed')
										})
								} else {
									console.log('no $smartapi.auth')
									this.$smartapi._getQuery.call(this, propName)
								}*/
								this.$smartapi._getQuery.call(this, propName)
								
							}
						}

						//var cbd = debounce(100, true, cb)
						var cb = debounce(100, true, queryCallback.bind(this) )

						if(typeof query.variables === 'function') {

							//console.log('query.variables: function')

							var variableWatcher = this.$watch(query.variables.bind(this), cb , {
								immediate: true,
								deep: true,
							})

							smartapi.watchers.push(variableWatcher)

							preFetch = false
						}

						if(typeof query.url === 'function') {
							var queryWatcher = this.$watch(query.url.bind(this), cb, {
								immediate: true,
								deep: true,
							})

							smartapi.watchers.push(queryWatcher)

							preFetch = false
						}


						if(preFetch) {
							console.log('preFetch:', propName)
							this.$smartapi._getQuery.call(this, propName)
						}

					}


				}
			},

			destroyed () {
				registerInstance(this)

				if (isDef(this.$options.api)) {
					//console.log('DESTROY')
					var smartapi = this._smartapiRoot._smartapi
					for(let x in smartapi.watchers) {
						let unwatch = smartapi.watchers[x]
						unwatch()
					}
				}
			},

		})

		Object.defineProperty(Vue.prototype, '$smartapi', {
			get () {
				return this._smartapiRoot._smartapi
			}
		})

	}

	constructor(options = {}) {
		//console.log('constructor:smartapi', options, this)
		this.app = null
		this.options = options
		this.axios = null
		if(options.axios)
			this.axios = options.axios
		this.cache = {}
		if(options.cache)
			this.cache = options.cache
		this.auth = null
		if(options.auth)
			this.auth = options.auth
	}

	init (app) {
		// main app already initialized.

		// console.log('smartapi:init', this.options, this)

		if (this.app) {
			return
		}

		this.app = app
		this.queries = {}
		this.watchers = []
		// this.queries[key] = 
	}

	_getQuery(propName) {

		let store = this.$store
		//console.log('HAVE VALID TOKEN', store.getters.haveValidToken)

		let options = this.options
		let smartData = this._smartapiData
		let smartApi = this._smartapiRoot._smartapi

		console.log('fetchData', smartData)

		var dataQuery = smartData[propName]
		var variables = {}

		if(typeof dataQuery.variables === 'function') {
			variables = dataQuery.variables.call(this)
		} else {
			variables = dataQuery.variables
		}

		if(typeof dataQuery.url_handler === 'function') {
			// console.log('fetchProperty: function')
			dataQuery.url_handler.call(this, variables )
				.then(result => {
					//console.log('FETCHED', result.url, result.data)
					this[propName] = result.data
				})
				.catch(error => {
					console.error('failed to fetch', propName, error )	
				})
		} else {
			//console.log('fetchProperty: axios query')
			// axios query
			var query_raw = ''
			if(typeof dataQuery.url === 'function') {
				query_raw = dataQuery.url.call(this)
			} else {
				query_raw = dataQuery.url
			}

			let query = Mustache.render(query_raw, variables)

			smartApi.axios.get(query)
				.then(result => {
					//console.log('FETCHED', query, result.data)
					this[propName] = result.data

					/*if(smartApi.cache) {
						smartApi.cache.writeQuery(query, result.data)
					}*/
				})
				.catch(error => {
					console.error('failed to fetch', query, propName, error )	

					smartApi.auth.check()
						.then(result => {
							smartApi.axios.get(query)
							.then(result => {
								this[propName] = result.data
							})
							.catch(error => {
								console.error('smartapi.query: auth failed #2')
							})
						})
						.catch(error => {
							console.error('smartapi.query: auth failed #1')
						})
				})
		}

	}

	delete(mutate) {
		/*let args = {
			url: null,
			url_handler: null,
			variables: null,
			update: null
		}*/

		let options = this.options

		// update(cache, data)
		return new Promise( (resolve, reject) => {
			if(mutate.url === undefined) {
				reject(new Error('url for "delete" missing'))
			}

			let variables = {}
			if(typeof mutate.variables === 'function') {
				variables = mutate.variables.call(this)
			} else if (typeof mutate.variables === 'object') {
				variables = mutate.variables
			}

			let url = Mustache.render(mutate.url, variables)

			this.axios.delete(url)
				.then(result => {
					console.log('delete success:', result)

					if(typeof mutate.update === 'function') {
						mutate.update(this.cache, result.data, url)
					}

					resolve(result.data)
				})
				.catch(error => {
					console.log('delete fail:', error)
					reject(error)
				})

		})
	}

	create(mutate) {
		let options = this.options

		return new Promise( (resolve, reject) => {
			if(mutate.url === undefined) {
				reject(new Error('url for "create" missing'))
			}

			let variables = {}
			if(typeof mutate.variables === 'function') {
				variables = mutate.variables.call(this)
			} else if (typeof mutate.variables === 'object') {
				variables = mutate.variables
			}

			let url = Mustache.render(mutate.url, variables)

			let data = {}
			if(typeof mutate.data === 'object') {
				data = mutate.data
			}

			this.axios.put(url, data)
				.then(result => {
					console.log('create success:', result)

					if(typeof mutate.update === 'function') {
						mutate.update(this.cache, result.data, url)
					}

					resolve(result.data)
				})
				.catch(error => {
					console.log('create fail:', error)
					reject(error)
				})

		})
	}

	upload(mutate) {
		let options = this.options

		return new Promise( (resolve, reject) => {
			if(mutate.url === undefined) {
				reject(new Error('url for "upload" missing'))
			}

			let variables = {}
			if(typeof mutate.variables === 'function') {
				variables = mutate.variables.call(this)
			} else if (typeof mutate.variables === 'object') {
				variables = mutate.variables
			}

			let url = Mustache.render(mutate.url, variables)

			var files_field_name = 'files'
			if(typeof mutate.files_name === 'string') {
				files_field_name = mutate.files_name
			} else if (typeof mutate.files_name === 'function') {
				files_field_name = mutate.files_name.call(this)
			}

			if(typeof mutate.files !== 'object') {
				reject(new Error('files for "upload" missing'))
			}

			let data = new FormData();
			for(let i in mutate.files) {
				let file = mutate.files[i]
				data.append(files_field_name + '['+i+']', file)
			}

			if(typeof mutate.data === 'object') {
				//data = mutate.data
				for(let key in mutate.data) {
					let value = mutate.data[key]
					console.log('key', key, mutate.data[key])
					data.append(key, value)
				}
			}

			this.axios.post(url, data)
				.then(result => {
					console.log('upload success:', result)

					if(typeof mutate.update === 'function') {
						mutate.update(this.cache, result.data, url)
					}

					resolve(result.data)
				})
				.catch(error => {
					console.log('upload fail:', error)
					reject(error)
				})

		})
	}

	readQuery(query) {
		/*let args = {
			url: null,
			variables: null,
		}*/

	}

	writeQuery(query, data) {

	}

}

export default SmartApi