import Vue from 'vue'
import SmartApi from './plugin'
import InMemoryCache from '@js/smartapi/cache-inmemory'
import Endpoints from '@api/endpoints'

import axios from 'axios'
import auth   from '@js/auth'
//import store  from '@js/store'

Vue.use(SmartApi)

const smartapi = new SmartApi({
	auth: auth,
	//store: store,
	axios: axios,
	cache: new InMemoryCache()
})

export default smartapi