export default class SmartQuery {

	constructor (self, smartapi, key, options) {
		this.self = self
		this.key = key
		this.smartapi = smartapi
		this.options = Object.assign({}, options)
	}

	refetch() {
		this.smartapi._getQuery.call(this.self, this.key)
	}

}
