import Vue from 'vue'
import SmartWamp from './plugin'
import Endpoints from '@api/endpoints'

Vue.use(SmartWamp)

const smartwamp = new SmartWamp({
	debug: true,
	url: 'ws://127.0.0.1:9000/',
	realm: 'fabui',

	onopen: (session) => {
		console.log('WAMP opened', this)
		/*session.call('greeting', [])
			.then(res => {
				console.log('Result:', res)
			})*/

		/*
			 // 1) subscribe to a topic
			 function onevent(args) {
					console.log("Event:", args[0]);
			 }
			 session.subscribe('com.myapp.hello', onevent);

			 // 2) publish an event
			 session.publish('com.myapp.hello', ['Hello, world!']);

			 // 3) register a procedure for remoting
			 function add2(args) {
					return args[0] + args[1];
			 }
			 session.register('com.myapp.add2', add2);

			 // 4) call a remote procedure
			 session.call('com.myapp.add2', [2, 3]).then(
					function (res) {
						 console.log("Result:", res);
					}
			 );
			 */
	    /*session.call('gcode.send', [], {gcode:'G91'})
	      .then(result => {
	          console.log('relative mode', result)
	      })
	      .catch(error => {
	          console.error('fail', error)
	      })*/

      	session.call('wamp.service.ping')
      		.then(result => {
      			console.log('normal:', result)
      		})
      		.catch(error => {
      			console.log('normal-fail:', error)
      		})

		session.subscribe('gcode.events', (args, kwargs) => {
			console.log('gcode.event:', args, kwargs)
		})

		session.subscribe('gcode.file.events', (args, kwargs) => {
			console.log('gcode.file.event:', args, kwargs)
		})

		session.subscribe('task.events', (args, kwargs) => {
			console.log('task.event:', args, kwargs)
		})

	},

	onclose: (reason) => {
		console.log('WAMP closed', reason)
	}

})

export default smartwamp