import abjs from 'autobahn'
import defer from 'deferred'

export let _Vue
export let install = {
	installed: false
}

class SmartWamp {

	static install(Vue, options) {

	//console.log('plugin.install')

		if (install.installed && _Vue === Vue) return
		install.installed = true

		_Vue = Vue

		const isDef = v => v !== undefined

		const registerInstance = (vm, callVal) => {
	
			let i = vm.$options._parentVnode
			if (isDef(i) && isDef(i = i.data) && isDef(i = i.registerRouteInstance)) {
				i(vm, callVal)
			}
		}

		Vue.mixin({
			beforeCreate () {

				if (isDef(this.$options.smartwamp)) {
					this._smartwamp = this.$options.smartwamp
					this._smartwampRoot = this
					this._smartwamp.init(this)
				} else {
					this._smartwampRoot = (this.$parent && this.$parent._smartwampRoot) || this
				}

				registerInstance(this, this)
			},

			create() {
				//
			},

			destroyed () {
				registerInstance(this)
			},

		})

		Object.defineProperty(Vue.prototype, '$wamp', {
			get () { 
				return this._smartwampRoot._smartwamp
			}
		})

	}

	constructor(options = {}) {
		this.app = null
		this.options = options
	}

	init (app) {
		// main app already initialized.
		if (this.app) {
			return
		}

		this.app = app
		this.session = null
		this.lost = false
		this.connection = null
		this.creadentials = {}
		this.queue = []
	}

	/* Private function */
	_defer(session, fn, args) {

		let d = defer()

		let q = {name:fn, args: args}
		q.defer = d

		if(session) {
			return session[fn].apply(session, args)
		} else {
			this.queue.push(q)
		}

		return d.promise
	}

	_opened(session) {
		//console.log('lets process the queue', session)
		for(let i in this.queue) {
			let q = this.queue[i]
			let fn = session[q.name]

			// console.log('trigger:', q.name, q.args)

			fn.apply(session, q.args)
				.then(r => {
					q.defer.resolve(r)
				})
				.catch(e => {
					q.defer.reject(e)
				})

			delete this.queue[i]
		}
	}

	/* Public functions */

	//var store = this.app.$store;
	open(creadentials={}) {
	
		let options = this.options
		var self = this
		if(creadentials) {
			this.creadentials = creadentials
		} else {
			creadentials = this.creadentials
		}
	
		var secret = creadentials.secret
		var authid = creadentials.username

		function onchallenge (session, method, extra) {
			//console.log('method', method, 'extra', extra)

			if (method == 'cryptosign') 
				return abjs.sign_challenge(secret, extra)
			else if (method == 'wampcra') 
				return abjs.auth_cra.sign(secret, extra.challenge)
			else if (method == 'token' || method == 'ticket')
				return secret
			else 
				throw 'don\'t know how to authenticate using \'' + method + '\''
		}

		var config = {
			url: options.url,
			realm: options.realm,
			authmethods: ['token'],
			authid: authid,
			onchallenge: onchallenge,
		}

		var conn = this.connection = new abjs.Connection(config)

		conn.onopen = function(session, details) {
			self.session = session
			if (options.hasOwnProperty('onopen') && typeof options['onopen'] === 'function') {
				options.onopen.apply(conn, [session, details])
			}
			self._opened(session)
		}

		conn.onclose = function (reason, details) {
			self.lost = 'lost' === reason
			if (options.hasOwnProperty('onclose') && typeof options['onclose'] === 'function') {
				options.onclose.apply(conn, [reason, details])
			}
		}

		conn.open()
	}

	close(reason='closed', message='') {
		if(this.connection.isOpen) {
			this.connection.close(reason, message)
		}
	}

	reconnect() {
		close('wamp.goodbye.reconnect')
		this.lost = true
		open()
	}

	call(procedure, args, kwargs, options) {
		let session = this.session
		console.log('$wamp.CALL', procedure)

		return this._defer(session, 'call', [procedure, args, kwargs, options])
	}

	subscribe(topic, handler, options) {
		let session = this.session
		return this._defer(session, 'subscribe', [topic, handler, options])
	}

	unsubscribe(subscription) {
		let session = this.session
		return this._defer(session, 'unsubscribe', [subscription])
	}

	publish(topic, args, kwargs, options) {
		let session = this.session
		return this._defer(session, 'publish', [topic, args, kwargs, options])
	}

	register(procedure, endpoint, options) {
		let session = this.session
		return this._defer(session, 'register', [procedure, endpoint, options])
	}

	unregister(registration) {
		let session = this.session
		return this._defer(session, 'unregister', [registration])
	}

}

export default SmartWamp