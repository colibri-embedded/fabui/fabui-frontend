import Vue from 'vue'
import VueRouter from 'vue-router'

// Error pages
import NotFound   from '@pages/NotFound/Index.vue'
import Forbidden  from '@pages/Forbidden/Index.vue'
import Unsupported from '@pages/Unsupported/Index.vue'
// Full view pages
import Setup      from '@pages/Setup/Index.vue'
import Login      from '@pages/Login/Index.vue'
// Dashboard pages
import Jog        from '@pages/Jog/Index.vue'
import Storage    from '@pages/Storage/Browse.vue'
import Development from '@pages/Development/Index.vue'
// Make
import MakePrint  from '@pages/Make/Print/Index.vue'

// Layout
import DashboardLayout from '@js/views/Layout/DashboardLayout.vue'

import store from  '@js/store'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/',
			redirect: {name: 'dashboard'}
		},

		{
			path: '/login',
			name: 'login',
			component: Login,
		},

		{
			path: '/setup',
			name: 'setup',
			component: Setup,
		},

		{
			path: '/dashboard',
			name: 'dashboard',
			redirect: to => {
				return {
					name: 'jog',
				}
			},

			component: DashboardLayout,
			children: [
				{
					path: 'jog', 
					name: 'jog', 
					component: Jog,
					meta: {
						auth: true
					}
				},
				{
					path: 'storage',
					name: 'storage-all',
					redirect: to => {
						return {
							name: 'storage',
							params: {
								storageType: 'local'
							}
						}
					},
					meta: {
						auth: true
					}
				},        
				{
					path: 'storage/:storageType/:fileId?',
					name: 'storage',
					props: true,
					component: Storage,
					meta: {
						auth: true
					}
				},

				{
					path: 'make/print/:storage?/:fileId?',
					name: 'make-print',
					props: true,
					component: MakePrint,
					meta: {
						auth: true
					}
				},

				{
					path: 'development',
					name: 'development',
					component: Development,
					meta: {
						auth: true
					}
				}
			]
		},

		{ path: '*', component: NotFound },

		{ 
			path: '/403', 
			name: '403', 
			component: Forbidden,
			meta: {
				auth: true,
			}
		},

		{
			path: '/unsupported',
			name: 'unsupported',
			component: Unsupported,
		},

	],
	linkActiveClass: 'active'
})

export default router