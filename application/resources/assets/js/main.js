require('./bootstrap')

import Vue from 'vue'
import Vuetify from 'vuetify'
import VueClipboard from 'vue-clipboard2'
import '@fortawesome/fontawesome-free/css/all.css'

import App from '@js/App'

import {bus}  from '@js/bus'
import auth   from '@js/auth'
import router from '@js/router'
import store  from '@js/store'
import i18n   from '@js/i18n'

import smartwamp from '@js/smartwamp'
import smartapi from '@js/smartapi'

// UI plugins
//import chartist from '@js/plugins/chartist'

Vue.use(Vuetify, {
	iconfont: 'fa'
})
Vue.use(VueClipboard)

import API from '@js/api'

new Vue({
	el: '#app',

	data: {

	},

	beforeCreate() {
		/*API.setup.check()
      .then(done => {
        if(!done) {
          this.$auth.logout()
          this.$router.push( {name: 'setup'} )
        } else {
          if( this.$auth.check(true) ) {
            console.log('is logged in')
          } else {
            console.log('not logged in')
            this.$auth.logout()
            this.$router.push( {name: 'login'} )
          }
        }
        console.log('setup-done', done)
      })
      .catch(error => {
        console.log('error: setup-check', error)
      })*/



		bus.$on('on-login', ()=> {
			console.log('LOGGED IN')
			// this.$wamp.open({
   //      		username: 'frontend',
   //      		secret: this.$store.getters.getToken
   //    		})
		})
		bus.$on('on-logout', ()=> {
			console.log('LOGGED OUT')
			//this.$wamp.close()
		})


		console.log('checking stored auth state')
		/*if( this.$auth.check(true) ) {
      console.log('is logged in')
      //this.$router.push( {name: 'dashboard'} )
    } else {
      console.log('not logged in')
      //this.$auth.logout()
      this.$router.push( {name: 'login'} )
    }*/
		this.$auth.check(true)
			.then(response => {
				console.log('is logged in')
				/*this.$wamp.open({
          username: 'peter',
          secret: 'secret2'
        })*/
			})
			.catch(error => {
				console.log('not logged in')
				this.$router.push( {name: 'login'} )
			})
	},

	created() {
      	/*this.$wamp.call('wamp.service.ping')
      		.then(result => {
      			console.log('premature:', result)
      		})
      		.catch(error => {
      			console.log('premature-fail:', error)
      		})*/
	},

	methods: {
		onLogin() {
      
		},

		onLogout() {
			console.log('LOGGED OUT')
		}
	},
	render: h => h(App),
	i18n,

	smartwamp,
	smartapi,

	store,
	router,
	auth,

	// data: {
	//   Chartist: Chartist
	// }
})