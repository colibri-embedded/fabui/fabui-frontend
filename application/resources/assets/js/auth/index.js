import Vue from 'vue'
import Auth from './plugin'
import Endpoints from '@api/endpoints'

import {bus} from '@js/bus'

Vue.use(Auth)

const auth = new Auth({
	auth: {
		grant_type: 'password',
		token_url: Endpoints.auth_token_url,
		refresh_url: Endpoints.auth_refresh_token_url,
		revoke_url: Endpoints.auth_revoke_url,
	},
	user_url: Endpoints.get_user_url,
	roles: 'role',
	on_login: () => {
		bus.$emit('on-login')
	},
	on_logout: () => {
		bus.$emit('on-logout')
	}
})

export default auth