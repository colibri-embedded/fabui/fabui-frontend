import axios from 'axios'

const Auth = {
	state : {
		user: {
			id: null,
			name: null,
			email: null,
			role: null,
		},
		token: {
			access_token: null,
			expires_in: null,
			refresh_token: null,
			token_type: null,
		},
	},

	mutations: {
		setUser(state, user) {
			state.user = {
				id: user.id,
				name: user.name,
				email: user.email,
				role: user.role,
			}
		},

		clearUser(state) {
			state.user = {
				id: null,
				name: null,
				email: null,
				role: null,
			}
		},

		updateTokens(state, tokens) {
			let now = Date.now()

			state.token.access_token = tokens.access_token
			state.token.expires_in = tokens.expires_in + now
			state.token.refresh_token = tokens.refresh_token
			state.token.token_type = tokens.token_type

			axios.defaults.headers.common['Authorization'] = 'Bearer ' + state.token.access_token
		},

		restoreToken(state) {
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + state.token.access_token
		},

		clearToken(state) {
			state.token = {
				access_token: null,
				expires_in: null,
				refresh_token: null,
				token_type: null,
			}

			delete axios.defaults.headers.common['Authorization']
		},
        
	},

	actions: {
	},

	getters: {
		user: (state, getters) => {
			return state.user
		},

		haveValidToken: (state, getters) => {
			if(state.token.access_token === null) {
				console.error('no access_token')
				return false
			}

			if(state.token.expires_in === null) {
				console.error('no expires_in')
				return false
			}

			let now = Date.now()
			if(now > state.token.expires_in) {
				console.error('token expired')
				return false
			}

			return true
		},

		tokenExpiresAt:  (state, getters) => {
			return state.token.expires_in	
		},

		getRefreshToken: (state, getters) => {
			return state.token.refresh_token
		},

		getToken: (state, getters) => {
			return state.token.access_token
		},

		haveValidUser: (state, getters) => {
			return state.user.id !== null
		},

		authorizationHeader: (state, getters) => {
			return 'Bearer ' + state.token.access_token
		},
	}

}

export default Auth