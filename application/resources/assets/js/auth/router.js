function beforeEach(auth, to, from, next) {
	const isDef = v => v !== undefined

	var authRedirect = auth.options.authRedirect || {name: 'login'}
	var forbiddenRedirect = auth.options.forbiddenRedirect || {name: '403'}

	// console.log('ROUTER: beforeEach')

	if( isDef(to.meta.auth) && to.meta.auth )  {

		//
		auth.check()
			.then(result => {
				// console.log('router.auth.check:', result)
				if( isDef(to.meta.model) && isDef(to.meta.operation) ) {
					if( !auth.can(to.meta.model, to.meta.operation) ) {
						next(forbiddenRedirect)
					}
				}
				next()
			})
			.catch(error => {
				// console.error('router.auth.check:', error)
				next( authRedirect )
			})

		return
	} else {
		// console.log('no authentication requirements')

		if( isDef(to.meta.model) && isDef(to.meta.operation) ) {
			if( !auth.can(to.meta.model, to.meta.operation) ) {
				next(forbiddenRedirect)
			}
		}
		next()

	}

}

export default {
	beforeEach: beforeEach
}