/**
 *
 * Generated code, DO NOT EDIT MANUALLY 
 *
 **/

const permissions = {
	// Task
	'Task': {
		'browse': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'OBSERVER', 'BACKEND' ],
		},
		'read': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'OBSERVER', 'BACKEND' ],
		},
		'add': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
		'edit': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
		'delete': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
	},
	// Setting
	'Setting': {
		'browse': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'OBSERVER', 'BACKEND' ],
		},
		'read': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'OBSERVER', 'BACKEND' ],
		},
		'add': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
		'edit': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
		'delete': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
	},
	// RefreshToken
	'RefreshToken': {
		'browse': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'OBSERVER', 'BACKEND' ],
		},
		'read': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'OBSERVER', 'BACKEND' ],
		},
		'add': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
		'edit': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
		'delete': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
	},
	// User
	'User': {
		'browse': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'OBSERVER', 'BACKEND' ],
		},
		'read': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'OBSERVER', 'BACKEND' ],
		},
		'add': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
		'edit': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
		'delete': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
	},
	// File
	'File': {
		'browse': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'OBSERVER', 'BACKEND' ],
		},
		'read': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'OBSERVER', 'BACKEND' ],
		},
		'add': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
		'edit': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
		'delete': {
			'access': [ 'owner', 'root' ],
			'roles': [ 'ADMIN', 'MANAGER', 'BACKEND' ],
		},
	},
}

export default permissions