import axios from 'axios'
import defer from 'deferred'
import permissions from './permissions'
import router_hooks from './router'

export let _Vue
export let install = {
	installed: false
}

class Auth {

	static install(Vue, options) {

		if (install.installed && _Vue === Vue) return
		install.installed = true

		_Vue = Vue


		const isDef = v => v !== undefined

		const registerInstance = (vm, callVal) => {
    
			let i = vm.$options._parentVnode
			if (isDef(i) && isDef(i = i.data) && isDef(i = i.registerRouteInstance)) {
				i(vm, callVal)
			}
		}

		Vue.mixin({
			beforeCreate () {
				if (isDef(this.$options.router)) {
					var auth = this._auth = this.$options.auth
					this._authRoot = this
					this._auth.init(this)

					this.$options.router.beforeEach((to, from, next) => {
						router_hooks.beforeEach(auth, to, from, next)
					})


				} else {
					this._authRoot = (this.$parent && this.$parent._authRoot) || this
				}
				registerInstance(this, this)
			},

			destroyed () {
				registerInstance(this)
			},

		})

		Object.defineProperty(Vue.prototype, '$auth', {
			get () { 
				return this._authRoot._auth
			}
		})

	}

	constructor(options = {}) {
		this.app = null
		this.options = options
	}

	init (app) {
		// main app already initialized.
		if (this.app) {
			return
		}

		this.app = app

		// console.log(this.options)

		this.onlogin = this.options.on_login
		this.onlogout = this.options.on_logout
		this.checking = false
		this.check_queue = []
		this.token_restored = false
	}

	login(data) {
		console.log('login', data)
		var store = this.app.$store
		let options = this.options

		return new Promise( (resolve, reject) => {
			var user = {
				email: data.email,
				password: data.password
			}
			this._getToken(user)
				.then(response => {
					store.commit('updateTokens', response.data)

					if(options.user_url) {
						axios.get(options.user_url)
							.then(response => {
								store.commit('setUser', response.data)
								if(this.onlogin)
									this.onlogin()
								resolve(response.data)
							})
							.catch(response => {
								reject(response)
							})

					}

				})
				.catch(response => {
					reject(response)
				})
		})

	}

	logout() {
		var store = this.app.$store
		let options = this.options

		if(options.auth.revoke_url) {
			axios.post(options.auth.revoke_url)
				.then(response => {
					//console.log('action resolve', response.data)
				})
				.catch(response => {
					//console.log('action reject', response)
				})
		}

		store.commit('clearToken')
		store.commit('clearUser')
		if(this.onlogout)
			this.onlogout()
	}

	check(fetchUser=false) {	

		if(this.checking) {
			console.log('AUTH.check defered')
			let d = defer()
			let q = {defer: defer}
			this.check_queue.push(q)

			console.log('QUEUE',this.check_queue)

			return d.promise
		}

		console.log('AUTH.check')

		this.checking = true
		console.log('checking = true')
		return this._check(fetchUser)
	}

	_check_reject(data) {
		console.log("Reject defered", this.check_queue)

		for(let i in this.check_queue) {
			let d = this.check_queue[i]
			d.defer.reject(data)
			delete this.check_queue[i]
		}

		this.checking = false
		console.log('checking = false')
	}

	_check_resolve(data) {
		console.log("Resolve defered", this.check_queue)

		for(let i in this.check_queue) {
			let q = this.check_queue[i]
			// console.log('queue:', q)
			q.defer.resolve(data)
			delete this.check_queue[i]
		}

		this.checking = false
		console.log('checking = false')
	}

	_check(fetchUser=false) {
		var self = this
		var store = this.app.$store
		let options = this.options

		return new Promise( (resolve, reject) => {

			let haveValidToken = store.getters.haveValidToken
			let now = Date.now()
			let diff = store.getters.tokenExpiresAt - now
			if( (diff) < 5000 ) {
				console.error('token expired', diff)
				haveValidToken = false
			}

			if(haveValidToken) {

				if(this.token_restored && !fetchUser) {
					resolve({})
					self._check_resolve({})
					return
				}

				console.log('has a valid access_token')

				store.commit('restoreToken')
				this.token_restored = true

				if(!store.getters.haveValidUser || fetchUser) {
					this.fetch(fetchUser)
						.then(response => {
							if(this.onlogin)
								this.onlogin()
							
							console.log('#1', response)
							self._check_resolve(response)
							resolve(response)
						})
						.catch(error => {
							let refresh_token = store.getters.getRefreshToken
							if(refresh_token !== null) {
								this._refreshToken(refresh_token)
									.then(response => {
										//console.log('success with refresh_token:', response)
										store.commit('updateTokens', response.data)
										this.fetch(true)
											.then(response => {
												if(this.onlogin)
													this.onlogin()
												resolve(response)
												console.log('#2', response)
												self._check_resolve(response)
											})
											.catch(error => {
												store.commit('clearToken')
												store.commit('clearUser')
												reject(error)
												self._check_reject(error)
											})
                        
									})
									.catch(error => {
										console.error('failed to refresh', error)
										store.commit('clearToken')
										store.commit('clearUser')
										reject(error)
										self._check_reject(error)
									})
							} else {
								reject(error)
								self._check_reject(error)
							}
						})
				} else {
					resolve({})

					self._check_resolve({})
				}
			} else {
				let refresh_token = store.getters.getRefreshToken
				if(refresh_token !== null) {
					//console.log('found expired access_token and a refresh_token')
					this._refreshToken(refresh_token)
						.then(response => {
							//console.log('success with refresh_token:', response)
							store.commit('updateTokens', response.data)
							this.fetch(true)
								.then(response => {
									if(this.onlogin)
										this.onlogin()
									resolve(response)
									console.log('#3', response)
									self._check_resolve(response)
								})
								.catch(error => {
									console.error('failed to refresh', error)
									store.commit('clearToken')
									store.commit('clearUser')
									reject(error)
									self._check_reject(error)
								})
                  
						})
						.catch(error => {
							console.error('failed to refresh', error)
							store.commit('clearToken')
							store.commit('clearUser')
							reject(error)
							self._check_reject(error)
						})
				} else {
					console.error('no valid tokens found')
					let e = {error:'no valid tokens found'}
					reject(e)
					self._check_reject(e)
				}
			}

		})

	}

	can(model_name, ability_name) {
		const isDef = v => v !== undefined
		// permissions

		if( isDef(permissions[model_name]) ) {
			let role = this._userRole()
			if(role) {
				if( isDef(permissions[model_name][ability_name]) ) {
					let rules = permissions[model_name][ability_name]
					if( rules.roles.indexOf(role) >= 0) {
						return true
					}
				}
			} 
		}

		console.log('forbidden', model_name, ability_name)

		return false
	}

	cannot(model_name, ability_name) {
		return !this.can(model_name, ability_name)
	}

	_userRole() {
		var store = this.app.$store
		var user = this.user()
		var roleField = this.options.roles || 'role'
		if( user[roleField] !== undefined ) {
			return user[roleField]
		}

		return null
	}

	_getToken(user) {
		let options = this.options
		return new Promise( (resolve, reject) => {
			let data = {
				grant_type: options.auth.grant_type,
				username: user.email,
				password: user.password,
				scope: '*'
			}

			// clear Authorization header while we access the token url
			delete axios.defaults.headers.common['Authorization'] // = 'Bearer ' + state.access_token

			axios.post(options.auth.token_url, data)
				.then(response => {
					//console.log('action resolve', response.data)
					resolve(response)
				})
				.catch(response => {
					//console.log('action reject', response)
					reject(response)
				})
		})
	}

	_refreshToken(refresh_token) {
		let options = this.options
		return new Promise( (resolve, reject) => {
			let data = {
				grant_type: 'refresh_token',
				refresh_token: refresh_token
			}

			// clear Authorization header while we access the token url
			delete axios.defaults.headers.common['Authorization'] // = 'Bearer ' + state.access_token

			axios.post(options.auth.refresh_url, data)
				.then(response => {
					//console.log('action resolve', response.data)
					resolve(response)
				})
				.catch(response => {
					//console.log('action reject', response)
					reject(response)
				})
		})
	}

	user() {
		var store = this.app.$store
		if(store.getters.haveValidUser) {
			return store.getters.user
		}
		return null
	}

	fetch(forceFetch=false) {
		var store = this.app.$store
		let options = this.options

		//console.log('auth.fetch')

		return new Promise( (resolve, reject) => {
			if(!store.getters.haveValidUser || forceFetch) {

				if(options.user_url) {
					//console.log('checking user')
					axios.get(options.user_url)
						.then(response => {
							//console.log('user OK')
							store.commit('setUser', response.data)
							resolve(response.data)
						})
						.catch(error =>{
							console.error('user fetch error', error)
							//store.commit('clearUser')
							//store.commit('clearToken')

							reject(response)
						}) 
				} else {
					let error = {
						message: 'missing auth.user_url option'
					}
					reject(error)
				}

			}
		})
	}

}

export default Auth