window._ = require('lodash')
window.Popper = require('popper.js').default

window.axios = require('axios')

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
window.compareVersions = require('compare-versions')
window.dateFormat = require('dateformat')

window.max_file_size = parseInt(document.head.querySelector('meta[name="max-file-size"]').content);
window.max_file_uploads = parseInt(document.head.querySelector('meta[name="max-file-uploads"]').content);

const { detect } = require('detect-browser')
window.browser = detect()
 
// handle the case where we don't detect the browser
/*if (browser) {
	console.log(browser.name);
	console.log(browser.version);
  	console.log(browser.os);
}*/