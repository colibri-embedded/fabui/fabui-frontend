<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">

    <!-- Upload limits -->
    <meta name="max-file-size" content="{{ $max_file_size }}">
    <meta name="max-file-uploads" content="{{ $max_file_uploads }}">
    
    <title>{{ config('app.name', 'FABUI') }}</title>

    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />

    <link href="{{ url('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <app></app>
    </div>

    <script src="{{ mix('js/main.js') }}"></script>
</body>
</html>