<?php

use Illuminate\Database\Seeder;

class FilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\User::class)->create([
        //     'email' => 'kesler.daniel@gmail.com',
        //     'password' => bcrypt('password'),
        //     'role' => 'ADMIN'
        // ]);
        // create 10 users using the user factory
        // factory(App\User::class, 10)->create();
        // LOGS
        $users = \App\User::all();
        foreach($users as $user) {
            $this->command->info( $user->name );
            $this->createFiles($user, 5, 2, null, 3);

            /*factory(\App\Log::class, $numberOfLogs)->create([
                'company_id' => $company->id,
                'user_id' => $user->id
            ]);*/
        }
    }

    public function createFiles($user, $number_of_files, $number_of_directories, $parent_id = null, $level = 5)
    {
        for($i = 0; $i<$number_of_files; $i++) {
            factory(App\File::class)->create([
                'user_id' => $user->id,
                'parent_id' => $parent_id
            ]);
        }

        if($level) {
            for($i = 0; $i<$number_of_directories; $i++) {
                $file = factory(App\File::class)->create([
                    'user_id' => $user->id,
                    'is_directory' => true,
                    'ext' => '',
                    'filename' => '',
                    'parent_id' => $parent_id
                ]);

                $new_level = $level - 1;

                $this->createFiles($user, $number_of_files, $number_of_directories, $file->id, $new_level);
            }
        }

    }
}