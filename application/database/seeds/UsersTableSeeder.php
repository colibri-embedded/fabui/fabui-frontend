<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'email' => 'kesler.daniel@gmail.com',
            'password' => bcrypt('password'),
            'role' => 'ADMIN'
        ]);

        factory(App\User::class)->create([
            'email' => 'manager@gmail.com',
            'password' => bcrypt('password'),
            'role' => 'MANAGER'
        ]);

        factory(App\User::class)->create([
            'email' => 'observer@future.time',
            'password' => bcrypt('password'),
            'role' => 'OBSERVER'
        ]);

        // create 10 users using the user factory
        //factory(App\User::class, 10)->create();
    }
}