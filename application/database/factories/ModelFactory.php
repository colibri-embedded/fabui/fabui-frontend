<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
	static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'role' => $faker->randomElement(['MANAGER', 'ADMIN', 'OBSERVER']),
        'avatar_url' => 'https://randomuser.me/api/portraits/' . $faker->randomElement(['men', 'women']) . '/' . rand(1,99) . '.jpg',
    ];
});

$factory->define(App\File::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'filename' => '/local/files/' . $faker->md5,
        'ext' => '.gcode',
        'is_directory' => false,
        'is_shared' => false,
        'user_id' => function() {
            return App\User::inRandomOrder()->first()->id;
        }
    ];
});