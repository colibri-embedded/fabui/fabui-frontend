let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/main.js', 'public/js')
	.stylus('resources/assets/stylus/app.styl', 'public/css', {
		use: [
			require('rupture')()
		]
	})
	.sourceMaps()
	.webpackConfig({
		resolve: {
			alias: {
				'@js': path.resolve(
					__dirname,
					'resources/assets/js'
				),
				'@api': path.resolve(
					__dirname,
					'resources/assets/js/api'
				),
				'@components': path.resolve(
					__dirname,
					'resources/assets/js/views/Components'
				),
				'@pages': path.resolve(
					__dirname,
					'resources/assets/js/views/Pages'
				)
			}
		}
	});